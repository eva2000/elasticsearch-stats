#!/bin/bash
#############################################
# xenforo enhanced search shell stats
#############################################
DT=$(date +"%d%m%y-%H%M%S")
CENTMINLOGDIR='/root/centminlogs'
INDEXSTATS_LOG="${CENTMINLOGDIR}/es_index_stats-${DT}.log"
NODESTATS_LOG="${CENTMINLOGDIR}/es_node_stats-${DT}.log"
NODE_LOG="${CENTMINLOGDIR}/es_node-${DT}.log"
ES_ENDPOINT='http://localhost:9200'
ES_INDEXNAME='xfes'

if [ ! -f /usr/bin/jq ]; then
  yum -q -y install jq
fi

if [ ! -d "$CENTMINLOGDIR" ]; then
  if [ -d /etc/centminmod ]; then
    mkdir -p "$CENTMINLOGDIR"
  else
    CENTMINLOGDIR='/home/centminlogs'
    INDEXSTATS_LOG="${CENTMINLOGDIR}/es_index_stats-${DT}.log"
    NODESTATS_LOG="${CENTMINLOGDIR}/es_node_stats-${DT}.log"
    NODE_LOG="${CENTMINLOGDIR}/es_node-${DT}.log"
    mkdir -p "$CENTMINLOGDIR"
  fi
fi

getes_version() {
  curl -sX GET "${ES_ENDPOINT}/_nodes/" > "$NODE_LOG"
  es_version=$(cat $NODE_LOG | jq -r '.nodes[] | .version')
  es_compression=$(cat $NODE_LOG | jq -r '.nodes[] | .settings.index.codec')
  es_diskthreshold_enabled=$(cat $NODE_LOG | jq -r '.nodes[] | .settings.cluster.routing.allocation.disk.threshold_enabled')
  es_diskwatermark_low=$(cat $NODE_LOG | jq -r '.nodes[] | .settings.cluster.routing.allocation.disk.watermark.low')
  es_diskwatermark_high=$(cat $NODE_LOG | jq -r '.nodes[] | .settings.cluster.routing.allocation.disk.watermark.high')
  es_diskwatermark_floodstage=$(cat $NODE_LOG | jq -r '.nodes[] | .settings.cluster.routing.allocation.disk.watermark.flood_stage')
  persist_es_diskthreshold_enabled=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.persistent.cluster.routing.allocation.disk.threshold_enabled')
  persist_es_diskwatermark_low=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.persistent.cluster.routing.allocation.disk.watermark.low')
  persist_es_diskwatermark_high=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.persistent.cluster.routing.allocation.disk.watermark.high')
  persist_es_diskwatermark_floodstage=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.persistent.cluster.routing.allocation.disk.watermark.flood_stage')
  transient_es_diskthreshold_enabled=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.transient.cluster.routing.allocation.disk.threshold_enabled')
  transient_es_diskwatermark_low=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.transient.cluster.routing.allocation.disk.watermark.low')
  transient_es_diskwatermark_high=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.transient.cluster.routing.allocation.disk.watermark.high')
  transient_es_diskwatermark_floodstage=$(curl -sX GET "${ES_ENDPOINT}/_cluster/settings" | jq -r '.transient.cluster.routing.allocation.disk.watermark.flood_stage')
  echo "elasticsearch-version: $es_version"
}

getindex_stats() {
  curl -sX GET "${ES_ENDPOINT}/$ES_INDEXNAME/_stats/" > "$INDEXSTATS_LOG"
  allocatedmem=$(echo "scale=2; $(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.segments.memory_in_bytes")/1024" | bc)
  totalsegments=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.segments.count")
  totaldocs=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.docs | \"\(.count)\"")
  totalindex=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.indexing | \"\(.index_total)\"")
  totalindextime=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.indexing | \"\(.index_time_in_millis)\"")
  totaldocsize=$(echo "scale=2; $(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.store.size_in_bytes")/1024" | bc)
  searchindex=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.search | \"\(.query_total)\"")
  searchindextime=$(cat $INDEXSTATS_LOG | jq -r ".indices.\"${ES_INDEXNAME}\".total.search | \"\(.query_time_in_millis)\"")
  if [[ "$totalindextime" = '0' || "$totalindex" = '0' ]]; then
    avg_indextime=0
  elif [[ "$totalindextime" != 'null' && ! -z "$totalindex" ]]; then
    avg_indextime=$(echo "scale=2; $totalindextime/$totalindex" | bc)
  fi
  if [[ "$searchindextime" = '0' || "$searchindex" = '0' ]]; then
    avg_searchtime=0
  elif [[ "$searchindextime" != 'null' && ! -z "$searchindex" ]]; then
    avg_searchtime=$(echo "scale=2; $searchindextime/$searchindex" | bc)
  fi

  if [[ "$searchindex" = 'null' ]]; then
    searchindex=0
    avg_searchtime=0
  fi
  if [[ "$searchindextime" = 'null' ]]; then
    searchindextime=0
    avg_searchtime=0
  fi
  if [[ "$totalindex" = 'null' ]]; then
    totalindex=0
    avg_indextime=0
  fi
  if [[ "$totalindextime" = 'null' ]]; then
    totalindextime=0
    avg_indextime=0
  fi

  echo "total-segments-count: $totalsegments"
  echo "total-docs: $totaldocs"
  echo "total-dics-size-kb: $totaldocsize"
  echo "total-docs-allocated-memory: ${allocatedmem}KB"
  echo "total-index-updates: $totalindex"
  echo "total-index-time-ms: $totalindextime"
  echo "total-index-searches: $searchindex"
  echo "total-index-searches-time-ms: $searchindextime"
  echo "avg-index-time-ms: $avg_indextime"
  echo "avg-search-time-ms: $avg_searchtime"
  echo "yaml-cluster-disk-threshold-enabled: $es_diskthreshold_enabled"
  echo "yaml-cluster-disk-watermark-low: $es_diskwatermark_low"
  echo "yaml-cluster-disk-watermark-high: $es_diskwatermark_high"
  echo "yaml-cluster-disk-watermark-floodstage: $es_diskwatermark_floodstage"
  echo "persistent-cluster-disk-threshold-enabled: $persist_es_diskthreshold_enabled"
  echo "persistent-cluster-disk-watermark-low: $persist_es_diskwatermark_low"
  echo "persistent-cluster-disk-watermark-high: $persist_es_diskwatermark_high"
  echo "persistent-cluster-disk-watermark-floodstage: $persist_es_diskwatermark_floodstage"
  echo "transient-cluster-disk-threshold-enabled: $transient_es_diskthreshold_enabled"
  echo "transient-cluster-disk-watermark-low: $transient_es_diskwatermark_low"
  echo "transient-cluster-disk-watermark-high: $transient_es_diskwatermark_high"
  echo "transient-cluster-disk-watermark-floodstage: $transient_es_diskwatermark_floodstage"
}

getnode_stats() {
  curl -sX GET "${ES_ENDPOINT}/_nodes/stats/" > "$NODESTATS_LOG"
  uptime=$(cat $NODESTATS_LOG| jq -r '.nodes[] | .jvm.uptime_in_millis')
  heap_used_mem=$(echo "scale=2; $(cat $NODESTATS_LOG| jq -r '.nodes[] | .jvm.mem.heap_used_in_bytes')/1024/1024" | bc)
  heap_max_mem=$(echo "scale=2; $(cat $NODESTATS_LOG| jq -r '.nodes[] | .jvm.mem.heap_max_in_bytes')/1024/1024" | bc)
  heap_pc=$(echo "scale=2; $heap_used_mem/$heap_max_mem*100" | bc)

  fs_path=$(cat $NODESTATS_LOG| jq -r '.nodes[] | .fs.data[] | .path')
  fs_pathsize=$(du -sh  /var/lib/elasticsearch/nodes/0 | awk '{print $1}')
  fs_type=$(cat $NODESTATS_LOG| jq -r '.nodes[] | .fs.data[] | .type')
  fs_disksize=$(echo "scale=2; $(cat $NODESTATS_LOG| jq -r '.nodes[] | .fs.data[] | .total_in_bytes')/1024/1024/1024" | bc)
  fs_diskfreesize=$(echo "scale=2; $(cat $NODESTATS_LOG| jq -r '.nodes[] | .fs.data[] | .available_in_bytes')/1024/1024/1024" | bc)
  fs_diskusedsize=$(echo "scale=2; $fs_disksize-$fs_diskfreesize" | bc)
  fs_diskpc=$(echo "scale=2; $fs_diskusedsize/$fs_disksize*100" | bc)

  echo "uptime-ms: $uptime"
  echo "elasticsearch-index-codec: $es_compression"
  echo "elasticsearch-used-memory-percentage: $heap_pc"
  echo "elasticsearch-used-memory-mb: $heap_used_mem"
  echo "elasticsearch-max-memory-mb: $heap_max_mem"
  echo "elasticsearch-fs-path: $fs_path"
  echo "elasticsearch-fs-path-size: $fs_pathsize"
  echo "elasticsearch-fs-type: $fs_type"
  echo "elasticsearch-fs-disk-used-percentage: $fs_diskpc"
  echo "elasticsearch-fs-disk-size-gb: $fs_disksize"
  echo "elasticsearch-fs-disk-used-size-gb: $fs_diskusedsize"
  echo "elasticsearch-fs-disk-free-size-gb: $fs_diskfreesize"

}

getcat(){
  echo
  echo "allocation:"
  curl -sX GET "${ES_ENDPOINT}/_cat/allocation?v"
  echo
  echo "shards:"
  curl -sX GET "${ES_ENDPOINT}/_cat/shards?v"
  echo
  curl -sX GET "${ES_ENDPOINT}/_cat/segments/${ES_INDEXNAME}?v"
}

getthreadpools() {
  echo
  echo "thread_pools:"
  curl -sX GET "${ES_ENDPOINT}/_cat/thread_pool?h=host,name,min,max,queue,queue_size&v&s=name"
}

getindex_health() {
  echo
  echo "info health:"
  curl -sX GET "${ES_ENDPOINT}/_cat/indices?v"
  echo
  curl -sX GET "${ES_ENDPOINT}/_cat/nodes?v"
  echo
  curl -sX GET "${ES_ENDPOINT}/_cat/nodes?v&h=name,indexing.index_total,indexing.index_time,search.query_total,search.query_time"

}



{
  if [ -z "$1" ]; then
    ES_INDEXNAME=$ES_INDEXNAME
  else
    checkindex_exists=$(curl -sX GET "${ES_ENDPOINT}/$1" | jq -r '.status')
    if [[ "$checkindex_exists" -eq '404' ]]; then
      echo
      echo "elasticsearch index named = $1 does not exist"
      exit 1
    fi
    ES_INDEXNAME=$1
  fi
  {
    getes_version
    getnode_stats
    getindex_stats
  } | column -t
  getindex_health
  getcat
  getthreadpools
  echo
  echo "raw log saved: $INDEXSTATS_LOG"
  echo "raw log saved: $NODESTATS_LOG"
  echo "raw log saved: $NODE_LOG"
  echo "summary log saved: ${CENTMINLOGDIR}/elasticsearch-stats-${DT}.log"
} 2>&1 | tee "${CENTMINLOGDIR}/elasticsearch-stats-${DT}.log"
exit