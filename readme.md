# ElasticSearch Statistics For Xenforo Enhanced Search

Written for [Centmin Mod LEMP CentOS 6/7 usage](https://centminmod.com).

You pass the elasticsearch index name (set in Xenforo Enhanced Search addon admin settings) on the command line i.e. `xfes`

```
/root/tools/es_stats/es_stats.sh xfes                                           
elasticsearch-version:                         6.6.0
uptime-ms:                                     111091308
elasticsearch-index-codec:                     best_compression
elasticsearch-used-memory-percentage:          67.00
elasticsearch-used-memory-mb:                  345.32
elasticsearch-max-memory-mb:                   512.00
elasticsearch-fs-path:                         /var/lib/elasticsearch/nodes/0
elasticsearch-fs-path-size:                    852K
elasticsearch-fs-type:                         ext4
elasticsearch-fs-disk-used-percentage:         19.00
elasticsearch-fs-disk-size-gb:                 49.08
elasticsearch-fs-disk-used-size-gb:            9.44
elasticsearch-fs-disk-free-size-gb:            39.64
total-segments-count:                          24
total-docs:                                    78
total-dics-size-kb:                            183.93
total-docs-allocated-memory:                   36.32KB
total-index-updates:                           202
total-index-time-ms:                           2216
total-index-searches:                          605
total-index-searches-time-ms:                  8550
avg-index-time-ms:                             10.97
avg-search-time-ms:                            14.13
yaml-cluster-disk-threshold-enabled:           true
yaml-cluster-disk-watermark-low:               90%
yaml-cluster-disk-watermark-high:              95%
yaml-cluster-disk-watermark-floodstage:        null
persistent-cluster-disk-threshold-enabled:     null
persistent-cluster-disk-watermark-low:         null
persistent-cluster-disk-watermark-high:        null
persistent-cluster-disk-watermark-floodstage:  null
transient-cluster-disk-threshold-enabled:      null
transient-cluster-disk-watermark-low:          null
transient-cluster-disk-watermark-high:         null
transient-cluster-disk-watermark-floodstage:   null

info health:
health status index uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   xfes  Plm19LnQRVqZ58K8yAeb2Q   5   0         78           20    183.9kb        183.9kb

ip        heap.percent ram.percent cpu load_1m load_5m load_15m node.role master name
127.0.0.1           67          99  10    0.59    0.44     0.31 mdi       *      xfnode

name   indexing.index_total indexing.index_time search.query_total search.query_time
xfnode                  790                3.5s                635                9s

allocation:
shards disk.indices disk.used disk.avail disk.total disk.percent host      ip        node
     5      183.9kb     9.4gb     39.6gb       49gb           19 localhost 127.0.0.1 xfnode

shards:
xfes 4 p STARTED 17 36.3kb 127.0.0.1 xfnode
xfes 3 p STARTED 18 55.1kb 127.0.0.1 xfnode
xfes 2 p STARTED 12 39.7kb 127.0.0.1 xfnode
xfes 1 p STARTED 17 24.7kb 127.0.0.1 xfnode
xfes 0 p STARTED 14 27.8kb 127.0.0.1 xfnode

index shard prirep ip        segment generation docs.count docs.deleted   size size.memory committed searchable version compound
xfes  0     p      127.0.0.1 _5               5         10            0 10.6kb        1863 true      true       7.6.0   false
xfes  0     p      127.0.0.1 _6               6          2            2  6.2kb        1484 true      true       7.6.0   true
xfes  0     p      127.0.0.1 _8               8          1            0  4.7kb        1257 true      true       7.6.0   true
xfes  0     p      127.0.0.1 _9               9          1            1  5.6kb        1484 true      true       7.6.0   true
xfes  1     p      127.0.0.1 _7               7         16            0 18.5kb        2223 true      true       7.6.0   false
xfes  1     p      127.0.0.1 _8               8          1            1  5.7kb        1484 true      true       7.6.0   true
xfes  2     p      127.0.0.1 _4               4          8            0  9.2kb        1975 true      true       7.6.0   false
xfes  2     p      127.0.0.1 _5               5          1            0  6.8kb        1257 true      true       7.6.0   true
xfes  2     p      127.0.0.1 _8               8          1            0 11.3kb        1276 true      true       7.6.0   true
xfes  2     p      127.0.0.1 _9               9          1            1    6kb        1484 true      true       7.6.0   true
xfes  2     p      127.0.0.1 _a              10          1            1  5.7kb        1484 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _1               1         10            2 12.1kb        1935 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _2               2          1            0  4.4kb        1482 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _3               3          2            2  5.4kb        1484 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _j              19          1            0  8.7kb        1262 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _k              20          1            1  5.4kb        1484 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _l              21          1            1  5.9kb        1484 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _m              22          1            1  5.8kb        1484 true      true       7.6.0   true
xfes  3     p      127.0.0.1 _n              23          1            1  6.2kb        1484 true      true       7.6.0   true
xfes  4     p      127.0.0.1 _6               6         11            0 11.9kb        1892 true      true       7.6.0   false
xfes  4     p      127.0.0.1 _7               7          2            2  6.4kb        1484 true      true       7.6.0   true
xfes  4     p      127.0.0.1 _8               8          2            2    6kb        1484 true      true       7.6.0   true
xfes  4     p      127.0.0.1 _9               9          1            1  5.6kb        1484 true      true       7.6.0   true
xfes  4     p      127.0.0.1 _a              10          1            1  5.6kb        1484 true      true       7.6.0   true

thread_pools:
host      name                min max queue queue_size
localhost analyze               1   1     0         16
localhost ccr                  32  32     0        100
localhost fetch_shard_started   1   4     0         -1
localhost fetch_shard_store     1   4     0         -1
localhost flush                 1   1     0         -1
localhost force_merge           1   1     0         -1
localhost generic               4 128     0         -1
localhost get                   2   2     0       1000
localhost index                 2   2     0        200
localhost listener              1   1     0         -1
localhost management            1   5     0         -1
localhost ml_autodetect        80  80     0         80
localhost ml_datafeed          20  20     0        200
localhost ml_utility           80  80     0        500
localhost refresh               1   1     0         -1
localhost rollup_indexing       4   4     0          4
localhost search                4   4     0       1000
localhost search_throttled      1   1     0        100
localhost security-token-key    1   1     0       1000
localhost snapshot              1   1     0         -1
localhost warmer                1   1     0         -1
localhost watcher              10  10     0       1000
localhost write                 2   2     0        200

raw log saved: /root/centminlogs/es_index_stats-190219-074406.log
raw log saved: /root/centminlogs/es_node_stats-190219-074406.log
raw log saved: /root/centminlogs/es_node-190219-074406.log
summary log saved: /root/centminlogs/elasticsearch-stats-190219-074406.log
```